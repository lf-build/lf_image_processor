"use strict";

var bunyan = require("bunyan");
var express = require('express');
var multiparty = require('multiparty');
var sharp = require('sharp');
var fs = require('fs');

var RequestContext = require("./RequestContext");
var RequestLogger = require("./RequestLogger");

class Startup {

    constructor() {
        this.services = {
            levelStringToStream :function() {
                return {
                    write: log => {
            
                        // Map int log level to string
                        const clonedLog = Object.assign({}, log, {
                            level: bunyan.nameFromLevel[log.level]
                        });
            
                        var logLine = JSON.stringify(clonedLog, bunyan.safeCycles()) + '\n';
                        process.stdout.write(logLine);
                    }
                };
            },
            logger: function () {
                return bunyan.createLogger({
                    name: "image-processor",
                    service:'image-processor',
                    streams: [{
                        type: 'raw',
                        stream: this.levelStringToStream()
                      }]
                });
            },
            requestLogger: function () {
                return new RequestLogger(this.logger());
            }
        };
    }

    createRoutes(server) {
        var self = this;

        var resizeImageHander = function (request, response) {
            var width = parseInt(request.params.width);
            var height = parseInt(request.params.height);
            var form = new multiparty.Form();
            if (width == null || isNaN(width) || width <= 0) {
                response.writeHead(400, { 'content-type': 'text/plain' });
                response.end("invalid request. width is not specified.");
                return;
            }
            if (height == null || isNaN(height) || height <= 0) {
                response.writeHead(400, { 'content-type': 'text/plain' });
                response.end("invalid request. height is not specified.");
                return;
            }
            form.parse(request, function (err, fields, files) {
                if (err) {
                    response.writeHead(400, { 'content-type': 'text/plain' });
                    response.end("invalid request: " + err.message);
                    return;
                }
                if (files == null || files.file == null || files.file.length <= 0) {
                    response.writeHead(400, { 'content-type': 'text/plain' });
                    response.end("invalid request. file is not specified.");
                    return;
                }
                var file = files.file[0];
                fs.createReadStream(file.path)
                    .pipe(sharp()
                        .resize(width, height)
                        .png())
                    .pipe(response);
            });

        };

        server.get("/ping", function (request, response) {
            response.send(200, 'pong');
        });

        server.post("/resize/:width/:height", resizeImageHander);
    }

    run() {

        var self = this;

        var server = express();

        server.use(function (request, response, next) {
            self.services.requestContext = function () {
                return new RequestContext(request);
            };
            return next();
        });

        server.use(function (request, response, next) {
            var log = self.services.requestLogger();
            try {
                next();
            } catch (error) {
                log.error(request, response, error);
                return;
            }
            log.info(request, response);
        });

        this.createRoutes(server);

        server.listen(5000, function () {
            self.services.logger().info("server started");
        });

    }
}

new Startup().run();